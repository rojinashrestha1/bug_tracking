﻿using ClassLibrary1;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    public partial class Tester : Form
    {
        public Tester()
        {
            InitializeComponent();
        }

        BLLProject blp = new BLLProject();
        BLLTester blt = new BLLTester();

        private void Tester_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'bugtrakingDataSet1.tblBugDetail' table. You can move, or remove it, as needed.
            this.tblBugDetailTableAdapter.Fill(this.bugtrakingDataSet1.tblBugDetail);
            DataTable dt = blp.GetallProject();
            if (dt.Rows.Count > 0)
            {
                cboProject.DataSource = dt;
                cboProject.DisplayMember = "Project_title";
                cboProject.ValueMember = "Project_ID";
            }

            Loadgrid();



            cboSeverity.SelectedIndex = 0;
            cboStatus.SelectedIndex = 0;
            cboProject.Focus();
        }

        private void Loadgrid()
        {
            DataTable dt = blt.GetallBugs();
            dataGridView1.DataSource = dt;
        }

        private void btnSubmit_Click(object sender, EventArgs e)
        {

            int i = blt.Submitt(Convert.ToInt32(cboProject.SelectedValue.ToString()), txtBugtitle.Text, txtBugDescription.Text, cboSeverity.Text, cboStatus.Text, Convert.ToDateTime(dateTimePicker1.Text));


            if (i > 0)
            {


                MessageBox.Show("Bug Submitted Successfully");
                Loadgrid();



            }
        }



    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    public partial class Main : Form
    {
        public Main()
        {
            InitializeComponent();
        }

        private void createToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Tester frm = new Tester();
            frm.Show();
        }

        private void addUserToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SignUp frm = new SignUp();
            frm.Show();
            
        }

        private void userListToolStripMenuItem_Click(object sender, EventArgs e)
        {
            UserList frm = new UserList();
            frm.Show();
        }

        private void addProjectToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Project frm = new Project();
            frm.Show();
        }

        private void productListToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Project_List frm = new Project_List();
            frm.Show();
        }

        private void bugListToolStripMenuItem_Click(object sender, EventArgs e)
        {
            BugList frm = new BugList();
            frm.Show();
        }

        private void logOutToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            AssignedList frm = new AssignedList();
            frm.Show();
        }

        private void logoutToolStripMenuItem3_Click(object sender, EventArgs e)
        {
            ResolvedBugs frm = new ResolvedBugs();
            frm.Show();
            
        }

        private void logoutToolStripMenuItem4_Click(object sender, EventArgs e)
        {
            Login frm = new Login();
            frm.Show();
            this.Hide();
        }

      

     
    }
}

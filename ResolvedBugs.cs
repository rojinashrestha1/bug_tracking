﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    public partial class ResolvedBugs : Form
    {
        public ResolvedBugs()
        {
            InitializeComponent();
        }

        private void ResolvedBugs_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'bugtrakingDataSet6.tblResolvedBugs' table. You can move, or remove it, as needed.
            this.tblResolvedBugsTableAdapter.Fill(this.bugtrakingDataSet6.tblResolvedBugs);

        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void dataGridView1_RowHeaderMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
         
        }
    }
}

﻿using ClassLibrary1;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    public partial class TeamLeader : Form
    {
        public TeamLeader()
        {
            InitializeComponent();
        }
        public int bugid = 0;
        BLLUser blu = new BLLUser();
        BLLTeamLeader blt = new BLLTeamLeader();

        private void TeamLeader_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'bugtrakingDataSet4.tblBugAssign' table. You can move, or remove it, as needed.

            cboAssignto.DataSource = blu.GetDeveloperByUsertype();
            cboAssignto.DisplayMember = "Username";
            cboAssignto.ValueMember = "User_Id";
            //LoadGrid();

        }

        //public void LoadGrid()
        //{


        //    dataGridView1.DataSource = null;

        //    dataGridView1.DataSource = blt.GetAllAssignedBugs();


        //}

        private void btnAssign_Click(object sender, EventArgs e)
        {
            int i = blt.Assign(Convert.ToInt32(txtBugID.Text), txtProject.Text, txtBugtitle.Text, txtBugDescription.Text, cboAssignto.SelectedValue.ToString(), Convert.ToDateTime(dateTimePicker1.Value), cboStatus.Text, txtSeverity.Text);

            
            if (i > 0)
            {


                MessageBox.Show("Bug Assigned Successfully");
                //LoadGrid();
                

            }
        }

        private void txtBugtitle_TextChanged(object sender, EventArgs e)
        {

        }
    }
}

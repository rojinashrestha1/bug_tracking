﻿using ClassLibrary1;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    public partial class UserList : Form
    {
        public UserList()
        {
            InitializeComponent();
        }
        BLLUser blu = new BLLUser();
        private void UserList_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'bugtrakingDataSet.tblNewuser' table. You can move, or remove it, as needed.
            this.tblNewuserTableAdapter.Fill(this.bugtrakingDataSet.tblNewuser);

        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            DataTable dt = blu.GetUserByUserType(txtSearch.Text);
            if (dt.Rows.Count > 0)
            {
                dataGridView1.DataSource = dt;
            }
            else
            {
                MessageBox.Show("No Record Found");
            } 
        }

        private void txtSearch_TextChanged(object sender, EventArgs e)
        {
            DataTable dt = blu.GetUserByUserType(txtSearch.Text);
            if (dt.Rows.Count > 0)
            {
                dataGridView1.DataSource = dt;
            }
        }


      

        private void dataGridView1_RowHeaderMouseDoubleClick_1(object sender, DataGridViewCellMouseEventArgs e)
        {
            EditUser frm = new EditUser();


            frm.userid = Convert.ToInt32(dataGridView1.CurrentRow.Cells[0].Value.ToString());
            frm.txtName.Text = dataGridView1.CurrentRow.Cells[1].Value.ToString();
            frm.txtAddress.Text = dataGridView1.CurrentRow.Cells[2].Value.ToString();
            frm.cboGender.Text = dataGridView1.CurrentRow.Cells[3].Value.ToString();
            frm.txtEmail.Text = dataGridView1.CurrentRow.Cells[4].Value.ToString();
            frm.txtPhoneno.Text = dataGridView1.CurrentRow.Cells[5].Value.ToString();

            frm.txtUsername.Text = dataGridView1.CurrentRow.Cells[6].Value.ToString();
            frm.cboUsertype.Text = dataGridView1.CurrentRow.Cells[7].Value.ToString();
            frm.txtPassword.Text = dataGridView1.CurrentRow.Cells[8].Value.ToString();


            frm.Show();
            this.Hide();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

    }
}

﻿using ClassLibrary1;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    public partial class Login : Form
    {
        public Login()
        {
            InitializeComponent();
        }

        BLLUser blu = new BLLUser();

        private void btnLogin_Click(object sender, EventArgs e)
        {
            {
                DataTable dt = blu.checkUserLogin(txtUsername.Text, txtPassword.Text, cboUsertype.Text);
                if (dt.Rows.Count > 0)
                {
                   
                    if (cboUsertype.Text == "Tester")
                    {
                        MessageBox.Show("Welcome to Bug Tracking Software");

                        Main frm = new Main();
                        frm.Show();
                        this.Hide();

                    }
                    else if (cboUsertype.Text == "Team Leader")
                    {
                        MessageBox.Show("Welcome to Bug Tracking Software");

                        Main frm = new Main();
                        frm.Show();
                    }
                    else if (cboUsertype.Text == "Developer")
                    {
                        MessageBox.Show("Welcome to Bug Tracking Software");

                        Main frm = new Main();
                        frm.Show();
                    }
                }
                else
                {
                    MessageBox.Show("invalid username and password");
                }
            }
        }

     

        private void btncancel_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void Login_Load(object sender, EventArgs e)
        {
 
         
            cboUsertype.SelectedIndex = 0;
            txtUsername.Focus();
        }
    }
}


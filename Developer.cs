﻿using ClassLibrary1;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    public partial class Developer : Form
    {
        public Developer()
        {
            InitializeComponent();
        }
        BLLDeveloper bld = new BLLDeveloper();
        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            int i = bld.Save(Convert.ToInt32(txtBugID.Text), txtProject.Text, txtBugtitle.Text, txtBugDescription.Text, txtline.Text, txtcomment.Text, cboStatus.Text);
            if (i>0)
            {
                MessageBox.Show("Bug resolved successfully.");
            }
        }
    }
}

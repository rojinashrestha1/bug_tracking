﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    public partial class AssignedList : Form
    {
        public AssignedList()
        {
            InitializeComponent();
        }

        private void AssignedList_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'bugtrakingDataSet5.tblBugAssign' table. You can move, or remove it, as needed.
            this.tblBugAssignTableAdapter.Fill(this.bugtrakingDataSet5.tblBugAssign);

        }

        private void dataGridView1_RowHeaderMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            Developer frm = new Developer();
            
            
            frm.txtBugID.Text = (dataGridView1.CurrentRow.Cells[1].Value.ToString());
            frm.txtProject.Text = dataGridView1.CurrentRow.Cells[2].Value.ToString();
            frm.txtBugtitle.Text = dataGridView1.CurrentRow.Cells[3].Value.ToString();
            frm.txtBugDescription.Text = dataGridView1.CurrentRow.Cells[4].Value.ToString();
           
            frm.cboStatus.Text = dataGridView1.CurrentRow.Cells[7].Value.ToString();


            frm.Show();
            this.Hide();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}

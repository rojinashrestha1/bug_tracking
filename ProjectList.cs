﻿using ClassLibrary1;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    public partial class Project_List : Form
    {
        public Project_List()
        {
            InitializeComponent();
        }

        BLLProject blp = new BLLProject();
        private void Project_List_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'bugtrakingDataSet2.tblProject' table. You can move, or remove it, as needed.
            this.tblProjectTableAdapter.Fill(this.bugtrakingDataSet2.tblProject);
            // TODO: This line of code loads data into the 'bugtrakingDataSet1.tbl_Project' table. You can move, or remove it, as needed.

        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            DataTable dt = blp.GetProjectByProtitle((txtSearch.Text));
            if (dt.Rows.Count > 0)
            {
                dataGridView1.DataSource = dt;
            }
            else
            {
                MessageBox.Show("No Record Found");
            }
        }

        private void txtSearch_TextChanged(object sender, EventArgs e)
        {
            DataTable dt = blp.GetProjectByProtitle(txtSearch.Text);
            if (dt.Rows.Count > 0)
            {
                dataGridView1.DataSource = dt;
            }
        }

        private void dataGridView1_RowHeaderMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            EditProject frm = new EditProject();
            frm.Project_ID = Convert.ToInt32(dataGridView1.CurrentRow.Cells[0].Value.ToString());
            frm.txtProject.Text = dataGridView1.CurrentRow.Cells[1].Value.ToString();
            frm.txtDesc.Text = dataGridView1.CurrentRow.Cells[2].Value.ToString();
            frm.Show();
            this.Hide();
            
        }
    }
}
